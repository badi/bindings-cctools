#!/usr/bin/env bash

set -e

module load cctools/3.6.1

ghc --make -package-db dist/package.conf.inplace -ldttools Example.hs
./Example
