{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include <cctools/debug.h>
module Bindings.CCTools.Debug where
import Foreign.Ptr
#strict_import

import Bindings.CCTools.IntSizes

#opaque_t __sFILE

#ccall cctools_debug , CLong -> CString -> IO ()
#ccall cctools_warn , CLong -> CString -> IO ()
#ccall cctools_fatal , CString -> IO ()
#ccall cctools_debug_config , CString -> IO ()
#ccall cctools_debug_config_file , CString -> IO ()
#ccall cctools_debug_config_file_size , CSize -> IO ()
#ccall cctools_debug_config_fatal , FunPtr (IO ()) -> IO ()
#ccall cctools_debug_config_getpid , FunPtr (CInt) -> IO ()
#ccall cctools_debug_flags_set , CString -> IO (CInt)
#ccall cctools_debug_flags_print , Ptr <__sFILE> -> IO ()
#ccall cctools_debug_flags_clear , IO (CLong)
#ccall cctools_debug_set_flag_name , CLong -> CString -> IO ()
#ccall cctools_debug_flags_restore , CLong -> IO ()
