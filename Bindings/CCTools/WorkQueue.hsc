{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include <cctools/work_queue.h>
module Bindings.CCTools.WorkQueue where
import Foreign.Ptr
#strict_import

import Bindings.CCTools.Timestamp

#opaque_t list
#opaque_t work_queue

#globalvar wq_option_fast_abort_multiplier , CDouble
#globalvar wq_option_scheduler , CInt
{- struct work_queue_task {
    char * tag;
    char * command_line;
    int worker_selection_algorithm;
    char * output;
    struct list * input_files;
    struct list * output_files;
    int taskid;
    int return_status;
    int result;
    char * host;
    char * hostname;
    timestamp_t time_task_submit;
    timestamp_t time_task_finish;
    timestamp_t time_app_delay;
    timestamp_t time_send_input_start;
    timestamp_t time_send_input_finish;
    timestamp_t time_execute_cmd_start;
    timestamp_t time_execute_cmd_finish;
    timestamp_t time_receive_output_start;
    timestamp_t time_receive_output_finish;
    long long total_bytes_transferred;
    timestamp_t total_transfer_time;
    timestamp_t cmd_execution_time;
}; -}
#starttype struct work_queue_task
#field tag , CString
#field command_line , CString
#field worker_selection_algorithm , CInt
#field output , CString
#field input_files , Ptr <list>
#field output_files , Ptr <list>
#field taskid , CInt
#field return_status , CInt
#field result , CInt
#field host , CString
#field hostname , CString
#field time_task_submit , CULong
#field time_task_finish , CULong
#field time_app_delay , CULong
#field time_send_input_start , CULong
#field time_send_input_finish , CULong
#field time_execute_cmd_start , CULong
#field time_execute_cmd_finish , CULong
#field time_receive_output_start , CULong
#field time_receive_output_finish , CULong
#field total_bytes_transferred , CLong
#field total_transfer_time , CULong
#field cmd_execution_time , CULong
#stoptype
{- struct work_queue_stats {
    int port;
    int priority;
    int workers_init;
    int workers_ready;
    int workers_busy;
    int workers_cancelling;
    int tasks_running;
    int tasks_waiting;
    int tasks_complete;
    int total_tasks_dispatched;
    int total_tasks_complete;
    int total_workers_joined;
    int total_workers_removed;
    long long total_bytes_sent;
    long long total_bytes_received;
    timestamp_t start_time;
    timestamp_t total_send_time;
    timestamp_t total_receive_time;
    double efficiency;
    double idle_percentage;
    int capacity;
    int avg_capacity;
    int total_workers_connected;
}; -}
#starttype struct work_queue_stats
#field port , CInt
#field priority , CInt
#field workers_init , CInt
#field workers_ready , CInt
#field workers_busy , CInt
#field workers_cancelling , CInt
#field tasks_running , CInt
#field tasks_waiting , CInt
#field tasks_complete , CInt
#field total_tasks_dispatched , CInt
#field total_tasks_complete , CInt
#field total_workers_joined , CInt
#field total_workers_removed , CInt
#field total_bytes_sent , CLong
#field total_bytes_received , CLong
#field start_time , CULong
#field total_send_time , CULong
#field total_receive_time , CULong
#field efficiency , CDouble
#field idle_percentage , CDouble
#field capacity , CInt
#field avg_capacity , CInt
#field total_workers_connected , CInt
#stoptype
#ccall work_queue_task_create , CString -> IO (Ptr <work_queue_task>)
#ccall work_queue_task_specify_file , Ptr <work_queue_task> -> CString -> CString -> CInt -> CInt -> IO ()
#ccall work_queue_task_specify_buffer , Ptr <work_queue_task> -> CString -> CInt -> CString -> CInt -> IO ()
#ccall work_queue_task_specify_file_command , Ptr <work_queue_task> -> CString -> CString -> CInt -> CInt -> IO ()
#ccall work_queue_task_specify_tag , Ptr <work_queue_task> -> CString -> IO ()
#ccall work_queue_task_specify_algorithm , Ptr <work_queue_task> -> CInt -> IO ()
#ccall work_queue_task_delete , Ptr <work_queue_task> -> IO ()
#ccall work_queue_create , CInt -> IO (Ptr <work_queue>)
#ccall work_queue_submit , Ptr <work_queue> -> Ptr <work_queue_task> -> IO (CInt)
#ccall work_queue_wait , Ptr <work_queue> -> CInt -> IO (Ptr <work_queue_task>)
#ccall work_queue_hungry , Ptr <work_queue> -> IO (CInt)
#ccall work_queue_empty , Ptr <work_queue> -> IO (CInt)
#ccall work_queue_port , Ptr <work_queue> -> IO (CInt)
#ccall work_queue_name , Ptr <work_queue> -> IO (CString)
#ccall work_queue_get_stats , Ptr <work_queue> -> Ptr <work_queue_stats> -> IO ()
#ccall work_queue_get_worker_summary , Ptr <work_queue> -> IO (CString)
#ccall work_queue_activate_fast_abort , Ptr <work_queue> -> CDouble -> IO (CInt)
#ccall work_queue_specify_algorithm , Ptr <work_queue> -> CInt -> IO ()
#ccall work_queue_specify_task_order , Ptr <work_queue> -> CInt -> IO ()
#ccall work_queue_specify_name , Ptr <work_queue> -> CString -> IO ()
#ccall work_queue_specify_priority , Ptr <work_queue> -> CInt -> IO ()
#ccall work_queue_specify_master_mode , Ptr <work_queue> -> CInt -> IO ()
#ccall work_queue_cancel_by_taskid , Ptr <work_queue> -> CInt -> IO (Ptr <work_queue_task>)
#ccall work_queue_cancel_by_tasktag , Ptr <work_queue> -> CString -> IO (Ptr <work_queue_task>)
#ccall work_queue_shut_down_workers , Ptr <work_queue> -> CInt -> IO (CInt)
#ccall work_queue_delete , Ptr <work_queue> -> IO ()
#ccall work_queue_specify_log , Ptr <work_queue> -> CString -> IO ()
#ccall work_queue_task_specify_input_buf , Ptr <work_queue_task> -> CString -> CInt -> CString -> IO ()
#ccall work_queue_task_specify_input_file , Ptr <work_queue_task> -> CString -> CString -> IO ()
#ccall work_queue_task_specify_input_file_do_not_cache , Ptr <work_queue_task> -> CString -> CString -> IO ()
#ccall work_queue_task_specify_output_file , Ptr <work_queue_task> -> CString -> CString -> IO ()
#ccall work_queue_task_specify_output_file_do_not_cache , Ptr <work_queue_task> -> CString -> CString -> IO ()

#num WORK_QUEUE_DEFAULT_PORT
#num WORK_QUEUE_RANDOM_PORT
#num WORK_QUEUE_WAITFORTASK
#num WORK_QUEUE_SCHEDULE_UNSET
#num WORK_QUEUE_SCHEDULE_FCFS
#num WORK_QUEUE_SCHEDULE_FILES
#num WORK_QUEUE_SCHEDULE_TIME
#num WORK_QUEUE_SCHEDULE_RAND
#num WORK_QUEUE_TASK_ORDER_FIFO
#num WORK_QUEUE_TASK_ORDER_LIFO
#num WORK_QUEUE_INPUT
#num WORK_QUEUE_OUTPUT
#num WORK_QUEUE_NOCACHE
#num WORK_QUEUE_CACHE
#num WORK_QUEUE_SYMLINK
#num WORK_QUEUE_PREEXIST
#num WORK_QUEUE_THIRDGET
#num WORK_QUEUE_THIRDPUT
#num WORK_QUEUE_MASTER_MODE_STANDALONE
#num WORK_QUEUE_MASTER_MODE_CATALOG
