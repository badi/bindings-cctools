{-# OPTIONS_GHC -fno-warn-unused-imports #-}
#include <bindings.dsl.h>
#include <cctools/timestamp.h>
module Bindings.CCTools.Timestamp where
import Foreign.Ptr
#strict_import

import Bindings.CCTools.IntSizes
{- typedef unsigned long long timestamp_t; -}
#synonym_t timestamp_t , CULong
#ccall timestamp_get , IO (CULong)
#ccall timestamp_fmt , CString -> CSize -> CString -> CULong -> IO (CInt)
#ccall timestamp_sleep , CULong -> IO ()
#ccall timestamp_file , CString -> IO (CLong)
