#!/usr/bin/env bash

ghci \
	-fbuilding-cabal-package \
	-odir dist/build \
	-hidir dist/build \
	-stubdir dist/build \
	-i -idist/build \
	-i. -idist/build/autogen \
	-Idist/build/autogen \
	-Idist/build \
	-optP-include -optPdist/build/autogen/cabal_macros.h \
	-package-name bindings-cctools-3.6.1.0.1.0.0 \
	-hide-all-packages \
	-no-user-package-db \
	-package-db cabal-dev/packages-7.6.1.conf \
	-package-db dist/package.conf.inplace \
	-package-id base-4.6.0.0-6898b0af758ec9881050c7cd4b3d7df3 \
	-package-id bindings-DSL-1.0.16-d91106736419f191ff5255071afca0fe \
	-XHaskell98 -XForeignFunctionInterface \
	Bindings.CCTools.Debug Bindings.CCTools.Timestamp Bindings.CCTools.IntSizes Bindings.CCTools.WorkQueue \
	-ldttools \
	$@