-- -*- indent-tabs-mode: nil; haskell-program-name: "./ghci.sh" -*- --

module Main where

import Bindings.CCTools.Debug
import Bindings.CCTools.WorkQueue

import Foreign.C
import Foreign.Ptr
import Foreign.Marshal (toBool)
import Foreign.Storable
import Control.Applicative ((<$>))


while mbool action = do
  b <- mbool
  if b
     then action >> while mbool action
     else return ()


task :: Show a => a -> IO (Ptr C'work_queue_task)
task i = do
  cmd  <- newCString $ "echo hello " ++ show i
  task <- c'work_queue_task_create cmd
  tag  <- newCString $ show i
  c'work_queue_task_specify_tag task tag
  return task


set_debug_flags :: [String] -> IO ()
set_debug_flags = mapM_ setFlag
    where setFlag f = newCString f >>= c'cctools_debug_flags_set

empty :: Ptr C'work_queue -> IO Bool
empty q = toBool <$> c'work_queue_empty q

main = do
  set_debug_flags ["all"]
  q     <- c'work_queue_create c'WORK_QUEUE_RANDOM_PORT
  p     <- c'work_queue_port q
  putStrLn $ "WorkQueue using port " ++ show p
  putStrLn $ "Creating tasks"
  tasks <- mapM task [1..10]
  putStrLn $ "Submitting tasks"
  mapM_ (c'work_queue_submit q) tasks
  while (not <$> empty q) $ do
         putStrLn $ "Waiting for result"
         t   <- c'work_queue_wait q (-1)
         tag <- peekCString =<< c'work_queue_task'tag <$> peek t
         putStrLn $ "Received task: " ++ show tag
         out <- peekCString =<< c'work_queue_task'output <$> peek t
         putStrLn $ out
  putStrLn $ "Completed tasks, shutting down"
