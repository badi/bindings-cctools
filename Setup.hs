import Distribution.Simple
import Distribution.Simple.Setup (fromFlag, configVerbosity, buildVerbosity)
import Distribution.Simple.Utils (rawSystemExit)
import Distribution.Verbosity (Verbosity)

main = defaultMainWithHooks myhooks

    where hook    = simpleUserHooks
          myhooks = hook

                    {

                      -- apply hsc patches
                      preBuild = \a f ->
                                 do
                                   patch Apply $ fromFlag $ buildVerbosity f
                                   preBuild hook a f

                      -- undo patches
                    , postBuild = \a f d i ->
                                  do
                                    patch UnApply $ fromFlag $ buildVerbosity f
                                    postBuild hook a f d i

                    }

data PatchDirection = Apply | UnApply

toShellFlags :: PatchDirection -> [String]
toShellFlags Apply = []
toShellFlags UnApply = ["-R"]


patch :: PatchDirection -> Verbosity -> IO ()
patch d v = rawSystemExit v "./apply-patches.sh" $ toShellFlags d
