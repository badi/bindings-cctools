#!/usr/bin/env sh

patch_flags="$@"

for p in patches/*.patch; do
    patch -p1 -i $p $patch_flags
done


### Emacs configuration
### http://www.gnu.org/software/emacs/manual/html_node/emacs/Specifying-File-Variables.html
# Local Variables:
#   indent-tabs-mode: nil
# End:
